package com.hero.wireless.web.entity.business;

import com.hero.wireless.web.entity.base.BaseEntity;
import java.util.Date;

public class AgentRoles extends BaseEntity {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_roles.Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_roles.Role_Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    private Integer role_Id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_roles.Agent_Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    private Integer agent_Id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_roles.Create_User
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    private String create_User;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_roles.Create_Date
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    private Date create_Date;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_roles.Id
     *
     * @return the value of agent_roles.Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_roles.Id
     *
     * @param id the value for agent_roles.Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_roles.Role_Id
     *
     * @return the value of agent_roles.Role_Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public Integer getRole_Id() {
        return role_Id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_roles.Role_Id
     *
     * @param role_Id the value for agent_roles.Role_Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public void setRole_Id(Integer role_Id) {
        this.role_Id = role_Id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_roles.Agent_Id
     *
     * @return the value of agent_roles.Agent_Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public Integer getAgent_Id() {
        return agent_Id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_roles.Agent_Id
     *
     * @param agent_Id the value for agent_roles.Agent_Id
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public void setAgent_Id(Integer agent_Id) {
        this.agent_Id = agent_Id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_roles.Create_User
     *
     * @return the value of agent_roles.Create_User
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public String getCreate_User() {
        return create_User;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_roles.Create_User
     *
     * @param create_User the value for agent_roles.Create_User
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public void setCreate_User(String create_User) {
        this.create_User = create_User;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_roles.Create_Date
     *
     * @return the value of agent_roles.Create_Date
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public Date getCreate_Date() {
        return create_Date;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_roles.Create_Date
     *
     * @param create_Date the value for agent_roles.Create_Date
     *
     * @mbg.generated Tue Oct 19 10:30:36 CST 2021
     */
    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }
}